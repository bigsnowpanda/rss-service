package main

import (
	"log"
	"net/http"

	"gitlab.com/rss-service/rss-server/router"
)

func main() {
	r := router.Router()
	log.Println("Starting server on the port 8080...")
	log.Fatal(http.ListenAndServe(":8080", r))
}
