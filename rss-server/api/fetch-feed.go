package api

import (
	rss "gitlab.com/rss-service/rss-server/rss-reader"
	"io"
	"net/http"
	"net/url"
	"time"
)

const httpClientTimeout = 10

func getAllFeeds(links []string) Response {

	resCh := make(chan FeedResult, len(links))
	for _, link := range links {
		go getFeed(link, resCh) // Run new goroutine for each link
	}

	res := Response{}
	res.Payload = make([]FeedResult, 0, len(links))
	for i := 0; i < len(links); i++ { // Waiting a response from all goroutines
		val := <-resCh
		res.Payload = append(res.Payload, val)
	}

	return res
}

func getFeed(link string, resCh chan FeedResult) {

	_, err := url.ParseRequestURI(link)
	if err != nil {
		resCh <- FeedResult{nil, errorMsg(link, err)} // Check is a string look like a valid URL path
	}

	net := &http.Client{
		Timeout: time.Second * httpClientTimeout,
	}
	res, err := net.Get(link)
	if err != nil {
		resCh <- FeedResult{nil, errorMsg(link, err)} // Any error during fetch URL and timeout
		return
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body) // Get data
	if err != nil {
		resCh <- FeedResult{nil, errorMsg(link, err)}
		return
	}

	rssFeed := &rss.RssFeed{
		Title: "",
		Link:  link,
		Items: nil,
	}
	rssFeed, err = parser(body) // Parsing data
	if err != nil {
		resCh <- FeedResult{nil, errorMsg(link, err)}
		return
	}

	resCh <- FeedResult{rssFeed, nil}
}
