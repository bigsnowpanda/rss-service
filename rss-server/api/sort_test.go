package api

import (
	"github.com/stretchr/testify/assert"
	rss "gitlab.com/rss-service/rss-server/rss-reader"
	"testing"
)

func TestSortBy_General(t *testing.T) {

	tRes := &Response{}

	tReqLinks := []string{"1", "2", "3"} // check reverse order
	tSortMode := SORT_BY_REQUEST
	err := tRes.sortBy(tReqLinks, tSortMode)
	assert.Nil(t, err)

	tSortMode = "TimE"
	err = tRes.sortBy(tReqLinks, tSortMode)
	assert.EqualError(t, err, sortModeNotImplemented.Error(), "")
}

func TestSortBy_SORT_BY_REQUEST(t *testing.T) {

	tRes := &Response{
		Payload: []FeedResult{
			{
				Feed: &rss.RssFeed{
					Title: "Title 1",
					Link:  "1",
					Items: nil,
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Title: "Title 2",
					Link:  "2",
					Items: nil,
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Title: "Title 3",
					Link:  "3",
					Items: nil,
				},
				ErrorMsg: nil,
			},
		},
	}

	tReqLinks := []string{"3", "2", "1"} // check reverse order
	tSortMode := SORT_BY_REQUEST
	err := tRes.sortBy(tReqLinks, tSortMode)
	assert.Nil(t, err)
	assert.Equal(t, len(tRes.Payload), 3)
	assert.Equal(t, tRes.Payload[0].Feed.Link, tReqLinks[0])
	assert.Equal(t, tRes.Payload[1].Feed.Link, tReqLinks[1])
	assert.Equal(t, tRes.Payload[2].Feed.Link, tReqLinks[2])
}

func TestSortBy_SORT_BY_TIME(t *testing.T) {

	tRes := &Response{
		Payload: []FeedResult{
			{
				Feed: &rss.RssFeed{
					Link: "1",
					Items: []rss.RssItem{
						{
							PublishDate: 1,
						},
					},
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Link: "2",
					Items: []rss.RssItem{
						{
							PublishDate: 9,
						},
					},
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Link: "3",
					Items: []rss.RssItem{
						{
							PublishDate: 7,
						},
					},
				},
				ErrorMsg: nil,
			},
		},
	}

	tReqLinks := []string{"1", "2", "3"} // check reverse order
	tSortMode := SORT_BY_TIME
	err := tRes.sortBy(tReqLinks, tSortMode)
	assert.Nil(t, err)
	assert.Equal(t, len(tRes.Payload), 3)
	assert.Equal(t, tRes.Payload[0].Feed.Link, tReqLinks[1])
	assert.Equal(t, tRes.Payload[1].Feed.Link, tReqLinks[2])
	assert.Equal(t, tRes.Payload[2].Feed.Link, tReqLinks[0])
}

func TestSortBy_SORT_BY_SIZE(t *testing.T) {

	tRes := &Response{
		Payload: []FeedResult{
			{
				Feed: &rss.RssFeed{
					Link: "1",
					Items: []rss.RssItem{
						{
							Title: "1",
						},
						{
							Title: "2",
						},
					},
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Link: "2",
					Items: []rss.RssItem{
						{
							Title: "1",
						},
						{
							Title: "2",
						},
						{
							Title: "3",
						},
					},
				},
				ErrorMsg: nil,
			},
			{
				Feed: &rss.RssFeed{
					Link: "3",
					Items: []rss.RssItem{
						{
							Title: "1",
						},
					},
				},
				ErrorMsg: nil,
			},
		},
	}

	tReqLinks := []string{"1", "2", "3"} // check reverse order
	tSortMode := SORT_BY_SIZE
	err := tRes.sortBy(tReqLinks, tSortMode)
	assert.Nil(t, err)
	assert.Equal(t, len(tRes.Payload), 3)
	assert.Equal(t, tRes.Payload[0].Feed.Link, tReqLinks[1])
	assert.Equal(t, tRes.Payload[1].Feed.Link, tReqLinks[0])
	assert.Equal(t, tRes.Payload[2].Feed.Link, tReqLinks[2])
}
