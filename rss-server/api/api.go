package api

import (
	"encoding/json"
	"log"
	"net/http"

	rss "gitlab.com/rss-service/rss-server/rss-reader"
)

var parser = rss.Parse

func FetchFeeds(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var req Request
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&req); err != nil {
		http.Error(w, "invalid request payload", http.StatusBadRequest)
		log.Printf("request parsing error: %v", err)
		return
	}
	defer r.Body.Close()

	res := getAllFeeds(req.Links)

	if err := res.sortBy(req.Links, req.SortBy); err != nil {
		http.Error(w, "invalid sort mode", http.StatusBadRequest)
		log.Printf("invalid sort_by value: %v", req.SortBy)
		return
	}

	if err := json.NewEncoder(w).Encode(res); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("parse result encode error: %v", err)
	}
}
