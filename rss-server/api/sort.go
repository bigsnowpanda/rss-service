package api

import (
	"errors"
	"sort"
)

const (
	SORT_BY_REQUEST = "list" // as in initial request list
	SORT_BY_TIME    = "time" // feeds with newest first
	SORT_BY_SIZE    = "size" // feeds with bigger items amount first
)

var sortModeNotImplemented = errors.New("sort mode not implemented")

// sortBy provide sorting result feeds by sort mode
// if sort mode defined but not implemented on server error return
// if sort mode not defined return feed list in order of request list
func (res *Response) sortBy(reqList []string, mode string) error {
	if len(mode) == 0 {
		mode = SORT_BY_REQUEST
	}

	switch mode {
	case SORT_BY_REQUEST: // as in initial request list
		sortByRequest(res, reqList)
	case SORT_BY_TIME: // feeds with newest first
		sortByTime(res, reqList)
	case SORT_BY_SIZE: // feeds with bigger items amount first
		sortBySize(res, reqList)
	default: // sort mode is defined but not yet implemented
		return sortModeNotImplemented
	}

	return nil
}

func sortByRequest(res *Response, reqList []string) {
	var updatedRes = &Response{
		Payload: make([]FeedResult, 0, len(reqList)),
	}
	feeds := res.Payload
	for i := range reqList {
		for ii := range feeds {
			if feeds[ii].Feed.Link == reqList[i] {
				updatedRes.Payload = append(updatedRes.Payload, feeds[ii])
			}
		}
	}
	*res = *updatedRes

}

func sortByTime(res *Response, reqList []string) {
	var updatedRes = &Response{
		Payload: make([]FeedResult, 0, len(reqList)),
	}
	type sortStructure struct {
		Link string
		Time int64
	}

	ss := make([]sortStructure, 0, len(reqList))
	for _, v := range res.Payload {

		if len(v.Feed.Items) > 0 {
			ss = append(ss, sortStructure{
				Link: v.Feed.Link,
				Time: v.Feed.Items[0].PublishDate, // use first item date as latest
			})
		} else {
			ss = append(ss, sortStructure{
				Link: v.Feed.Link,
				Time: 0, // use 0 if not defined
			})
		}
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Time > ss[j].Time
	})

	for _, v := range ss {
		item := FeedResult{}
		for i := range res.Payload {
			if res.Payload[i].Feed.Link == v.Link {
				item = res.Payload[i]
				break
			}
		}
		updatedRes.Payload = append(updatedRes.Payload, item)
	}
	*res = *updatedRes
}

func sortBySize(res *Response, reqList []string) {
	var updatedRes = &Response{
		Payload: make([]FeedResult, 0, len(reqList)),
	}
	type sortStructure struct {
		Link string
		Size int
	}

	ss := make([]sortStructure, 0, len(reqList))
	for _, v := range res.Payload {
		ss = append(ss, sortStructure{
			Link: v.Feed.Link,
			Size: len(v.Feed.Items),
		})
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Size > ss[j].Size
	})

	for _, v := range ss {
		item := FeedResult{}
		for i := range res.Payload {
			if res.Payload[i].Feed.Link == v.Link {
				item = res.Payload[i]
				break
			}
		}
		updatedRes.Payload = append(updatedRes.Payload, item)
	}
	*res = *updatedRes
}
