package api

func errorMsg(link string, err error) *ErrorMsg {
	return &ErrorMsg{Link: link, Msg: err.Error()}
}
