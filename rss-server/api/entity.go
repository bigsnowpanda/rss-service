package api

import rss "gitlab.com/rss-service/rss-server/rss-reader"

type Request struct {
	Links  []string `json:"links"`
	SortBy string   `json:"sort_by"`
}

type Response struct {
	Payload []FeedResult `json:"payload"`
}

type FeedResult struct {
	Feed     *rss.RssFeed `json:"feed"`
	ErrorMsg *ErrorMsg    `json:"error"`
}

type ErrorMsg struct {
	Link string `json:"link"`
	Msg  string `json:"msg"`
}
