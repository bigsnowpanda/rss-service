package router

import (
	"github.com/gorilla/mux"
	"gitlab.com/rss-service/rss-server/api"
)

// Router is exported and used in main.go
func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/feeds", api.FetchFeeds).Methods("POST")

	return router
}
