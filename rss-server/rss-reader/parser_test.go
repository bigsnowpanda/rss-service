package rss_reader

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParser_BasicTest(t *testing.T) {

	tXML := `
			<rss version="2.0">
				<channel>
					<title>FeedTitle</title>
					<link></link>
				</channel>
			</rss>
			`
	_, err := Parse([]byte(tXML))
	assert.Nil(t, err)

	tXML = `
			<rs version="2.0">
				<channel>
					<title>FeedTitle</title>
					<link></link>
				</channel>
			</rss>
			`
	_, err = Parse([]byte(tXML))
	assert.EqualError(t, err, invalidXmlFormat.Error())

	tXML = `
			<rss version="1.0">
				<channel>
					<title>FeedTitle</title>
					<link></link>
				</channel>
			</rss>
			`
	_, err = Parse([]byte(tXML))
	assert.EqualError(t, err, invalidRssFormat.Error())

}

func TestParser_ParsingTest(t *testing.T) {
	tXML := `
			<rss version="2.0">
				<channel>
					<title>Feed Title</title>
					<link></link>
						<item>
							<title>Item title 1</title>
							<link>Item link 1</link>
							<pubDate>Sat, 08 May 2021 00:00:00 +0000</pubDate>
							<description>Item description 1</description>
						</item>
				</channel>
			</rss>
			`
	feed, err := Parse([]byte(tXML))
	assert.Nil(t, err)
	assert.Equal(t, feed.Title, "Feed Title")
	assert.Equal(t, len(feed.Items), 1)
	assert.Equal(t, feed.Items[0].Title, "Item title 1")
	assert.Equal(t, feed.Items[0].Link, "Item link 1")
	assert.Equal(t, feed.Items[0].Description, "Item description 1")
	assert.Equal(t, feed.Items[0].PublishDate, int64(1620432000)) // check converting to timestamp
}
