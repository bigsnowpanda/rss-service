package rss_reader

import (
	"encoding/xml"
	"errors"
	"log"
)

var invalidXmlFormat = errors.New("invalid xml format")
var invalidRssFormat = errors.New("invalid rss format")

func Parse(data []byte) (*RssFeed, error) {
	feed := RssFeedXML{}

	err := xml.Unmarshal(data, &feed)
	if err != nil {
		log.Printf("xml parse error : %v", err)
		return nil, invalidXmlFormat
	}

	return xmlToFeed(feed)
}

// xmlToFeed convert parsed xml file to our RssFeed/RssItem structures
func xmlToFeed(feed RssFeedXML) (*RssFeed, error) {
	if feed.XMLName.Local != "rss" || feed.Version != "2.0" {
		return nil, invalidRssFormat
	}

	res := &RssFeed{}
	res.Title = feed.Title
	res.Items = make([]RssItem, 0, len(feed.ItemList))

	for i := range feed.ItemList {
		if feed.ItemList[i].Content != "" { // Usually used Description, but some times used Content as content receiver
			feed.ItemList[i].Description = feed.ItemList[i].Content
		}
		var timestamp int64
		t, err := parseTime(feed.ItemList[i].PubDate) // try to parse pub date to time.Size
		if err == nil {
			timestamp = t.Unix() // Convert time to timestamp
		} else {
			timestamp = 0 // If pub date is not assigned or parsed, use 0 value
		}

		newItem := RssItem{
			Title:       feed.ItemList[i].Title,
			Link:        feed.ItemList[i].Link,
			PublishDate: timestamp,
			Description: feed.ItemList[i].Description,
		}

		res.Items = append(res.Items, newItem)
	}

	return res, nil
}
