package rss_reader

import (
	"encoding/xml"
)

type RssFeed struct {
	Title string    `json:"title"`
	Link  string    `json:"link"`
	Items []RssItem `json:"items"`
}

type RssItem struct {
	Title       string `json:"title"`
	Source      string `json:"source"`
	SourceURL   string `json:"source_url"`
	Link        string `json:"link"`
	PublishDate int64  `json:"publish_timestamp"`
	Description string `json:"description"`
}

type RssFeedXML struct {
	XMLName xml.Name `xml:"rss"`
	Version string   `xml:"version,attr"`

	Title       string `xml:"channel>title"`
	Link        string `xml:"channel>link"`
	Description string `xml:"channel>description"`

	PubDate  string    `xml:"channel>pubDate"`
	ItemList []ItemXML `xml:"channel>item"`
}

type ItemXML struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`

	Content  string `xml:"encoded"`
	PubDate  string `xml:"pubDate"`
	Comments string `xml:"comments"`
}
