module gitlab.com/rss-service/rss-server

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
)
