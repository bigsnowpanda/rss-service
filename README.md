RSS Feed reading service
===

Service consists from 2 main parts server and front.

Most important parts has a tests (api and rss-reader packages)

Server listened localhost:8080

POST Request API with JSON in body
```
{
    "links":["link1","link2"],
    "sort_by":"SORT_MODE" (SORT_MODE=SORT_BY_REQUEST, SORT_BY_TIME or SORT_BY_SIZE)
}
```

API Response JSON

```
{
"payload":[
    {"feed":{
            "title":"Feed title 1",
            "link":"link1",
            "items":[
               {
                 "title":"Title 1...",
                 "link":"Item Link 1...",
                 "publish_timestamp":1620432000, // timestamp of pub date-time
                 "description":"Description..."
               }, ... // Next items
            }   
    },
    {"feed":{
            "title":"Feed title 2",
            "link":"link2",
            "items":[
               {
                 "title":"Title 2...",
                 "link":"Item Link 2...",
                 "publish_timestamp":1620432000, // timestamp of pub date-time
                 "description":"Description..."
               }, ... // Next items
            }   
   }]
}  
```


Manual running
=
Run server:
```
cd ./rss-server
go run main.go
```

Server test without front client.
 On new terminal run curl command:
```
curl --header "Content-Type: application/json" --request POST --data '{"links":["https://appliedgo.net/index.xml","https://hnrss.org/newest"],"sort_by":"time"}' http://localhost:8080/feeds
```

Run all server unit tests:
```
cd ./rss-server
go test -v ./...
```

Front-client (not fully implemented yet):
```
cd ./rss-front
yarn install
yarn dev
```